package com.epam.cloudx.exception;

public enum Resource {
    IMAGE,
    NOTIFICATION_SUBSCRIPTION
}
