package com.epam.cloudx.exception;

public class ImageExistsException extends ResourceException {
    private static final String MESSAGE = "Such image already exists";

    public ImageExistsException() {
        super(Resource.IMAGE, MESSAGE);
    }
}
