package com.epam.cloudx.exception;

public class SubscriptionExistsException extends ResourceException {
    private static final String MESSAGE = "Provided email already subscribed to notifications";

    public SubscriptionExistsException() {
        super(Resource.NOTIFICATION_SUBSCRIPTION, MESSAGE);
    }
}
