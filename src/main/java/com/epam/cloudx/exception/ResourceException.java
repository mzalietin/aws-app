package com.epam.cloudx.exception;

public class ResourceException extends RuntimeException {
    private final Resource type;

    public ResourceException(Resource type, String message) {
        super(message);
        this.type = type;
    }

    public Resource getType() {
        return type;
    }
}
