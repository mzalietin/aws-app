package com.epam.cloudx.exception;

public class ImageNotFoundException extends ResourceException {
    private static final String MESSAGE = "Failed to find image by name";

    public ImageNotFoundException() {
        super(Resource.IMAGE, MESSAGE);
    }
}
