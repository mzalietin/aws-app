package com.epam.cloudx.api;

import com.epam.cloudx.exception.ImageExistsException;
import com.epam.cloudx.exception.ImageNotFoundException;
import com.epam.cloudx.exception.ResourceException;
import com.epam.cloudx.exception.SubscriptionExistsException;
import java.util.HashMap;
import java.util.Map;
import javax.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({
            ImageExistsException.class,
            SubscriptionExistsException.class})
    public ResponseEntity<Object> handleResourceExists(ResourceException e) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(buildBody(e));
    }

    @ExceptionHandler(ImageNotFoundException.class)
    public ResponseEntity<Object> handleResourceNotFound(ResourceException e) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(buildBody(e));
    }

    private Map<String, String> buildBody(ResourceException e) {
        return Map.of("message", e.getMessage(), "resource", e.getType().name());
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException e) {
        var errors = new HashMap<>();
        e.getConstraintViolations().forEach(v -> errors.put(v.getPropertyPath(), v.getMessage()));
        return ResponseEntity.badRequest().body(errors);
    }
}
