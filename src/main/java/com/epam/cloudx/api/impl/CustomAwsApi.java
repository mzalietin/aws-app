package com.epam.cloudx.api.impl;

import com.epam.cloudx.dto.rs.Ec2Info;
import com.epam.cloudx.util.AwsUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/misc")
@RequiredArgsConstructor
public class CustomAwsApi {
    private final AwsUtil awsUtil;

    @GetMapping(path = "/ec2Info", produces = MediaType.APPLICATION_JSON_VALUE)
    public Ec2Info getEc2Metadata() {
        return awsUtil.getEc2Info();
    }

    @PostMapping(path = "/checkUploads", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> callUploadsNotifier() {
        var resp = awsUtil.invokeNotificationsProcessor();
        return ResponseEntity.ok().body(resp);
    }
}
