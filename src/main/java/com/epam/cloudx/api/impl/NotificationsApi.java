package com.epam.cloudx.api.impl;

import com.epam.cloudx.service.SubscriptionService;
import javax.validation.constraints.Email;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/notifications")
@Validated
@RequiredArgsConstructor
public class NotificationsApi {
    private final SubscriptionService subService;

    @PostMapping(path = "/sub/{email}")
    @ResponseStatus(HttpStatus.CREATED)
    public void addSubscription(@PathVariable @Email String email) {
        subService.subscribe(email);
    }

    @DeleteMapping(path = "/sub/{email}")
    public void deleteSubscription(@PathVariable @Email String email) {
        subService.unsubscribe(email);
    }
}
