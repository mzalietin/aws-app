package com.epam.cloudx.api.impl;

import com.epam.cloudx.entity.Image;
import com.epam.cloudx.event.ImageUploaded;
import com.epam.cloudx.service.ImageService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(path = "/image")
@RequiredArgsConstructor
public class ImageApi {
    private final ImageService imageService;
    private final ApplicationEventPublisher appEventPublisher;

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @SneakyThrows
    public void upload(@RequestParam("img") MultipartFile uploadFile) {
        var image = new Image(uploadFile);

        imageService.create(image);
        appEventPublisher.publishEvent(new ImageUploaded(image));
    }

    @GetMapping(path = "/{name}")
    public ResponseEntity<Object> download(@PathVariable("name") String imageName) {
        var image = imageService.get(imageName);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + imageName + "\"")
                .body(image.getContent());
    }

    @DeleteMapping(path = "/{name}")
    public void delete(@PathVariable("name") String imageName) {
        imageService.delete(imageName);
    }
}
