package com.epam.cloudx.api.impl;

import com.epam.cloudx.dto.rs.ImgMetadataResponse;
import com.epam.cloudx.dto.rs.ImgsMetadataResponse;
import com.epam.cloudx.service.ImageMetadataService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/metadata")
@RequiredArgsConstructor
public class MetadataApi {
    private final ImageMetadataService imageMetadataService;

    @GetMapping(path = "/images", produces = MediaType.APPLICATION_JSON_VALUE)
    public ImgsMetadataResponse getAllImagesMetadata() {
        return imageMetadataService.loadAll();
    }

    @GetMapping(path = "/image/random", produces = MediaType.APPLICATION_JSON_VALUE)
    public ImgMetadataResponse getRandomImageMetadata() {
        return imageMetadataService.loadRandom();
    }
}
