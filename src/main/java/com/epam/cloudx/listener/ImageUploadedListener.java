package com.epam.cloudx.listener;

import com.amazonaws.services.sqs.AmazonSQS;
import com.epam.cloudx.event.ImageUploaded;
import com.epam.cloudx.util.AwsUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URL;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class ImageUploadedListener {
    private final AmazonSQS amazonSqs;
    private final AwsUtil awsUtil;
    private final ObjectMapper jsonMapper;

    @Value("${notifications.sqs.queue}")
    private String queueUrl;

    @Value("${server.port}")
    private int applicationPort;

    @EventListener
    @Async
    @SneakyThrows
    public void handleImageUploaded(ImageUploaded event) {
        var imgLink = buildUrl(event.getImageName());
        var payload = new ImageInfo(event, imgLink);

        amazonSqs.sendMessage(queueUrl, jsonMapper.writeValueAsString(payload));

        log.info("Published ImageInfo[{}] to queue", event.getImageName());
    }

    @SneakyThrows
    private URL buildUrl(String imageName) {
        var instanceInfo = awsUtil.getEc2Info();
        return new URL("http", instanceInfo.getHost(), applicationPort, "/image/" + imageName);
    }

    @Getter
    @Setter
    @NoArgsConstructor
    static class ImageInfo {
        private String name;
        private String extension;
        private Float sizeMb;
        private String url;

        public ImageInfo(ImageUploaded event, URL imgUrl) {
            this.name = event.getImageName();
            this.extension = event.getExtension();
            this.sizeMb = event.getSizeMb();
            this.url = imgUrl.toString();
        }
    }
}
