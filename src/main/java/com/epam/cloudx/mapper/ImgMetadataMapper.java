package com.epam.cloudx.mapper;

import com.epam.cloudx.dto.rs.ImgMetadataResponse;
import com.epam.cloudx.entity.ImgMetadata;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ImgMetadataMapper {
    ImgMetadataResponse toResponse(ImgMetadata entity);
}
