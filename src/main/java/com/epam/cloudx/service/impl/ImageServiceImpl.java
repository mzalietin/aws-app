package com.epam.cloudx.service.impl;

import com.epam.cloudx.dto.rs.ImgMetadataResponse;
import com.epam.cloudx.dto.rs.ImgsMetadataResponse;
import com.epam.cloudx.entity.Image;
import com.epam.cloudx.exception.ImageExistsException;
import com.epam.cloudx.exception.ImageNotFoundException;
import com.epam.cloudx.mapper.ImgMetadataMapper;
import com.epam.cloudx.service.ImageMetadataService;
import com.epam.cloudx.service.ImageService;
import java.util.ArrayList;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
class ImageServiceImpl implements ImageService, ImageMetadataService {
    private final StorageService storageService;
    private final ImgMetadataRepo imgMetadataRepo;
    private final ImgMetadataMapper imgMetadataMapper;

    @Override
    public void create(Image image) {
        var filename = image.getMetadata().getName();

        if (imgMetadataRepo.existsById(filename)) {
            throw new ImageExistsException();
        }

        log.info("Creating image [{}] in system", filename);

        storageService.saveContent(image);
        imgMetadataRepo.save(image.getMetadata());
    }

    @Override
    public Image get(String name) {
        var metadata = imgMetadataRepo.findById(name)
                .orElseThrow(ImageNotFoundException::new);
        var imgBytes = storageService.fetchContent(name);

        return new Image(metadata, imgBytes);
    }

    @Override
    public void delete(String name) {
        if (!imgMetadataRepo.existsById(name)) {
            throw new ImageNotFoundException();
        }

        log.info("Deleting image [{}] from system", name);

        storageService.clear(name);
        imgMetadataRepo.deleteById(name);
    }

    @Override
    public ImgsMetadataResponse loadAll() {
        var imgs = new ArrayList<ImgMetadataResponse>();

        imgMetadataRepo.findAll()
                .forEach(m -> imgs.add(imgMetadataMapper.toResponse(m)));

        return new ImgsMetadataResponse(imgs);
    }

    @Override
    public ImgMetadataResponse loadRandom() {
        var imgs = loadAll().getImages();

        if (imgs.isEmpty()) {
            return new ImgMetadataResponse();
        }

        var index = (int) Math.floor(Math.random() * imgs.size());

        return imgs.get(index);
    }
}
