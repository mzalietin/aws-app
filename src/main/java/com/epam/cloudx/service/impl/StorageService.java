package com.epam.cloudx.service.impl;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.epam.cloudx.entity.Image;
import java.io.ByteArrayInputStream;
import java.net.URLConnection;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
class StorageService {
    private final AmazonS3 s3;

    @Value("${image.storage.bucket}")
    private String bucket;

    @Value("${image.storage.prefix}")
    private String folder;

    public void saveContent(Image image) {
        var name = image.getMetadata().getName();
        var content = image.getContent();
        var contentType = URLConnection.guessContentTypeFromName(name);

        var objectMetadata = new ObjectMetadata();
        objectMetadata.setContentLength(content.length);
        objectMetadata.setContentType(contentType);

        s3.putObject(bucket, folder + name, new ByteArrayInputStream(content), objectMetadata);

        log.info("Successfully pushed object [{}] to S3", name);
    }

    @SneakyThrows
    public byte[] fetchContent(String filename) {
        var object = s3.getObject(bucket, folder + filename);
        return object.getObjectContent().readAllBytes();
    }

    public void clear(String filename) {
        s3.deleteObject(bucket, folder + filename);

        log.info("Successfully removed object [{}] from S3", filename);
    }
}
