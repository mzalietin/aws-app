package com.epam.cloudx.service.impl;

import com.epam.cloudx.entity.ImgMetadata;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
interface ImgMetadataRepo extends CrudRepository<ImgMetadata, String> {
}
