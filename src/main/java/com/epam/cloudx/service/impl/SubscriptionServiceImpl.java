package com.epam.cloudx.service.impl;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.Subscription;
import com.epam.cloudx.exception.SubscriptionExistsException;
import com.epam.cloudx.service.SubscriptionService;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
class SubscriptionServiceImpl implements SubscriptionService {
    private static final String EMAIL_PROTOCOL = "email";

    private final AmazonSNS amazonSns;

    @Value("${notifications.sns.topic}")
    private String topicArn;

    @Override
    public void subscribe(String email) {
        var sub = findSub(email);

        if (sub.isEmpty()) {
            amazonSns.subscribe(topicArn, EMAIL_PROTOCOL, email);
        } else {
            throw new SubscriptionExistsException();
        }
    }

    @Override
    public void unsubscribe(String email) {
        var sub = findSub(email);
        sub.ifPresent(s -> amazonSns.unsubscribe(s.getSubscriptionArn()));
    }

    private Optional<Subscription> findSub(String email) {
        var subs = amazonSns.listSubscriptionsByTopic(topicArn).getSubscriptions();
        return subs.stream().filter(s -> s.getEndpoint().equals(email)).findAny();
    }
}
