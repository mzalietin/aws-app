package com.epam.cloudx.service;

public interface SubscriptionService {
    void subscribe(String email);

    void unsubscribe(String email);
}
