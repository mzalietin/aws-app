package com.epam.cloudx.service;

import com.epam.cloudx.entity.Image;

public interface ImageService {
    void create(Image image);

    Image get(String name);

    void delete(String name);
}
