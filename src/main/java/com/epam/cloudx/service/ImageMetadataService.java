package com.epam.cloudx.service;

import com.epam.cloudx.dto.rs.ImgMetadataResponse;
import com.epam.cloudx.dto.rs.ImgsMetadataResponse;

public interface ImageMetadataService {
    ImgsMetadataResponse loadAll();

    ImgMetadataResponse loadRandom();
}
