package com.epam.cloudx;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

;

@Configuration
public class AwsConfig {

    @Bean
    @Profile("ec2")
    public AWSCredentialsProvider instanceProfileCredProvider() {
        return new InstanceProfileCredentialsProvider(false);
    }

    @Bean
    @Profile("!ec2")
    public AWSCredentialsProvider localProfileCredProvider() {
        return new ProfileCredentialsProvider("cloudx_aws_app");
    }

    @Bean
    public AmazonS3 amazonS3(AWSCredentialsProvider credProvider) {
        return AmazonS3ClientBuilder.standard()
                .withRegion(Regions.EU_CENTRAL_1)
                .withCredentials(credProvider)
                .build();
    }

    @Bean
    public AmazonSQS amazonSqs(AWSCredentialsProvider credProvider) {
        return AmazonSQSClientBuilder.standard()
                .withRegion(Regions.EU_CENTRAL_1)
                .withCredentials(credProvider)
                .build();
    }

    @Bean
    public AmazonSNS amazonSns(AWSCredentialsProvider credProvider) {
        return AmazonSNSClientBuilder.standard()
                .withRegion(Regions.EU_CENTRAL_1)
                .withCredentials(credProvider)
                .build();
    }

    @Bean
    public AWSLambda awsLambda(AWSCredentialsProvider credProvider) {
        return AWSLambdaClientBuilder.standard()
                .withRegion(Regions.EU_CENTRAL_1)
                .withCredentials(credProvider)
                .build();
    }
}
