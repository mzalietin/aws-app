package com.epam.cloudx.dto.rs;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ImgsMetadataResponse {
    private List<ImgMetadataResponse> images;
}
