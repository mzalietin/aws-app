package com.epam.cloudx.dto.rs;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ImgMetadataResponse {
    private String name;
    private String extension;
    private Long size;
    private Long lastUpdated;
}
