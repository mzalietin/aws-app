package com.epam.cloudx.dto.rs;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Ec2Info {
    private String id;
    private String region;
    private String availabilityZone;
    private String host;
}
