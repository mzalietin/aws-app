package com.epam.cloudx.util;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.model.InvocationType;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.LogType;
import com.amazonaws.util.EC2MetadataUtils;
import com.epam.cloudx.dto.rs.Ec2Info;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AwsUtil {
    private final AWSLambda awsLambda;

    @Value("${notifications.lambda.processor}")
    private String uploadsBatchNotifierArn;

    private static final Ec2Info EC2_INFO;

    static {
        var id = EC2MetadataUtils.getInstanceId();
        var region = EC2MetadataUtils.getEC2InstanceRegion();
        var az = EC2MetadataUtils.getAvailabilityZone();
        var nwInterfaces = EC2MetadataUtils.getNetworkInterfaces();
        var host = nwInterfaces.isEmpty() ? "localhost" : nwInterfaces.get(0).getPublicIPv4s().get(0);

        EC2_INFO = new Ec2Info(id, region, az, host);
    }

    public String invokeNotificationsProcessor() {
        var invokeReq = new InvokeRequest()
                .withInvocationType(InvocationType.RequestResponse)
                .withFunctionName(uploadsBatchNotifierArn)
                .withLogType(LogType.None)
                .withQualifier("$LATEST");

        var result = awsLambda.invoke(invokeReq);

        return new String(result.getPayload().array());
    }

    public Ec2Info getEc2Info() {
        return EC2_INFO;
    }
}
