package com.epam.cloudx.entity;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.Objects;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@RequiredArgsConstructor
@Getter
public class Image {
    private final ImgMetadata metadata;
    private final byte[] content;

    public Image(MultipartFile file) throws IOException {
        var filename = file.getOriginalFilename();
        Objects.requireNonNull(filename);

        this.metadata = new ImgMetadata(filename, filename.split("\\.")[1],
                file.getSize(), ZonedDateTime.now().toEpochSecond());
        this.content = file.getBytes();
    }
}
