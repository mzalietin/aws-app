package com.epam.cloudx.entity;

import java.math.BigDecimal;
import java.math.MathContext;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ImgMetadata {
    @Id
    @Column(name = "name")
    private String name;

    @Column(name = "ext")
    private String extension;

    @Column(name = "size_in_bytes")
    private Long size;

    @Column(name = "last_updated")
    private Long lastUpdated;

    public Float sizeInMegabytes() {
        var sizeDec = new BigDecimal(size);
        var mbDec = new BigDecimal(1048576);
        var result = sizeDec.divide(mbDec);
        return result.round(new MathContext(2)).floatValue();
    }
}
