package com.epam.cloudx.event;

import com.epam.cloudx.entity.Image;
import lombok.Getter;

@Getter
public class ImageUploaded {
    private final String imageName;
    private final String extension;
    private final Float sizeMb;

    public ImageUploaded(Image image) {
        var metadata = image.getMetadata();
        this.imageName = metadata.getName();
        this.extension = metadata.getExtension();
        this.sizeMb = metadata.sizeInMegabytes();
    }
}
