# Training pet project with various Amazon Web Services integrations

## APIs

### Image

+ `POST /image` - upload image (as multipart file)
+ `GET /image/{name}` - download img by name
+ `DELETE /image/{name}` - delete img by name

Images content is saved to preconfigured S3 bucket and folder.
Also, metadata is recorded to database. <br>
H2 local setup:
- console url - `http://localhost:9090/h2-console`
- login, password, etc. - see `application-local.yml`

### Metadata

+ `GET /metadata/ec2` - get info about EC2 instance running on
+ `GET /metadata/images` - list all images metadata
+ `GET /metadata/image/random` - get metadata of random img

### Notifications

+ `POST /notifications/sub/{email}` - subscribe for uploads notifications
+ `DELETE /notifications/sub/{email}` - unsubscribe from uploads notifications

Notifications flow is following:

- `ImageUploaded` event emitted ->
- `ImageUploadedListener` publishes payload to SQS queue ->
- Lambda function `aws-app-uploads-batch-notifier` polls queue messages
and pushes one-per-batch notification to SNS topic ->
- Email is sent for those who have subscribed via `Notifications API`
